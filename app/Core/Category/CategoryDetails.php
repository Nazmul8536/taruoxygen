<?php
namespace App\Core\Category;

use App\Core\Category\Category;
use App\Models\Category as AppCategory;
use Illuminate\Contracts\Config\Repository;


class CategoryDetails implements Category{

    protected $config;
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    public function categorylist($categoryType = null)
    {
        // TODO: Implement categorylist() method.
    }

    public function addCategory(array $data)
    {
        // TODO: Implement addCategory() method.
    }

    public function editCategory($id, array $data)
    {
        // TODO: Implement editCategory() method.
    }

    public function deleteCategory($id)
    {
        // TODO: Implement deleteCategory() method.
    }

    public function getCategory()
    {
        // TODO: Implement getCategory() method.
    }
}
