<?php
namespace App\Core\Category;

use App\Models\Category as AppCategory;

interface Category{
    public function categorylist($categoryType = null);
    public function addCategory(array $data);
    public function editCategory($id,array $data);
    public function deleteCategory($id);
    public function getCategory();
}
