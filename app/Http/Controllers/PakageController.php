<?php

namespace App\Http\Controllers;

use App\Models\Pakage;
use Illuminate\Http\Request;

class PakageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pakage  $pakage
     * @return \Illuminate\Http\Response
     */
    public function show(Pakage $pakage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pakage  $pakage
     * @return \Illuminate\Http\Response
     */
    public function edit(Pakage $pakage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pakage  $pakage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pakage $pakage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pakage  $pakage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pakage $pakage)
    {
        //
    }
}
