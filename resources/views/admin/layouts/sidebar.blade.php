<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
        <a href="{{route('dashboard.index')}}">
            <img src="{{asset('./admin/images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
            <h5 class="logo-text">Dashtreme Admin</h5>
        </a>
    </div>
    <ul class="sidebar-menu do-nicescrol">
        <li class="sidebar-header">MAIN NAVIGATION</li>
        <li>
            <a href="{{route('dashboard.index')}}">
                <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
            </a>
            <ul></ul>
        </li>

        <li>
            <a href="{{route('category.index')}}">
                <i class="zmdi zmdi-collection-image-o"></i> <span>Category</span>
            </a>
        </li>

        <li>
            <a href="{{route('pakage.index')}}">
                <i class="zmdi zmdi-truck"></i> <span>Pakage</span>
            </a>
        </li>

        <li>
            <a href="{{route('products.index')}}">
                <i class="zmdi zmdi-mall"></i> <span>Product</span>
            </a>
        </li>


    </ul>

</div>
