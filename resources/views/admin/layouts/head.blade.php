<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Dashtreme Admin - Free Dashboard for Bootstrap 4 by Codervent</title>
    <!-- loader-->
    <link href="{{asset('./admin/css/pace.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('./admin/js/pace.min.js')}}"></script>
    <!--favicon-->
    <link rel="icon" href="{{asset('./admin/images/favicon.ico')}}" type="image/x-icon">
    <!-- Vector CSS -->
{{--    <link href="{{asset('./admin/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>--}}
<!-- simplebar CSS-->
    <link href="{{asset('./admin/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
    <!-- Bootstrap core CSS-->
    <link href="{{asset('./admin/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{asset('./admin/css/animate.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{asset('./admin/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="{{asset('./admin/css/sidebar-menu.css')}}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{asset('./admin/css/app-style.css')}}" rel="stylesheet"/>

</head>
