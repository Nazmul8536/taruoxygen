<script src="{{asset('./admin/js/jquery.min.js')}}"></script>
<script src="{{asset('./admin/js/popper.min.js')}}"></script>
<script src="{{asset('./admin/js/bootstrap.min.js')}}"></script>

<!-- simplebar js -->
<script src="{{asset('./admin/plugins/simplebar/js/simplebar.js')}}"></script>
<!-- sidebar-menu js -->
<script src="{{asset('./admin/js/sidebar-menu.js')}}"></script>
<!-- loader scripts -->
{{--<script src="{{asset('./admin/js/jquery.loading-indicator.js')}}"></script>--}}
<!-- Custom scripts -->
<script src="{{asset('./admin/js/app-script.js')}}"></script>
<!-- Chart js -->

<script src="{{asset('./admin/plugins/Chart.js/Chart.min.js')}}"></script>

<!-- Index js -->
<script src="{{asset('./admin/js/index.js')}}"></script>
