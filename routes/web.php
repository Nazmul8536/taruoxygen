<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//Dashboard
Route::get('dashboard/index','DashboardController@index')->name('dashboard.index');

//category
Route::get('category/index','CategoryController@index')->name('category.index');

//pakage
Route::get('pakage/index','PakageController@index')->name('pakage.index');

//products
Route::get('products/index','ProductController@index')->name('products.index');

//orders
Route::get('orders/index','OrderController@index')->name('orders.index');



